<?php


class BankAccountException extends Exception
{
}


class BankAccount
{
    private $balance = 0;

    public function __construct()
    {
        $this->balance = 0;
    }

    public function getBalance()
    {
        return $this->balance;
    }

    public function depositMoney($value)
    {
        if ($value < 0 || ! is_numeric($value)) {
            throw new BankAccountException("valor de deposito incorrecto", 1);
        }

        $this->balance += $value;
    }


    public function withdrawMoney($value)
    {
        if ($value < 0 || ! is_numeric($value) || $value > $this->balance) {
            throw new BankAccountException("valor de retiro incorrecto", 1);
        }

        $this->balance -= $value;
    }
}

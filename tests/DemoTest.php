<?php

declare(strict_types=1);
use PHPUnit\Framework\TestCase;

class DemoTest extends TestCase
{
    public function testEmpty()
    {
        $stack = [];
        $this->assertEmpty($stack);

        $variable = '';
        $this->assertEmpty($variable);

        return $stack;
    }

    /**
    * @test
     * @depends testEmpty
     */
    public function testPush(array $stack)
    {
        array_push($stack, 'foo');
        $this->assertEquals('foo', $stack[count($stack)-1]);
        $this->assertNotEmpty($stack);

        return $stack;
    }

    /**
     * @depends testPush
     */
    public function testPop(array $stack)
    {
        $this->assertEquals('foo', array_pop($stack));
        $this->assertEmpty($stack);
    }


    /**
    * @dataProvider additionProvider
    */
    public function testAdd($a, $b, $expected)
    {
        $this->assertEquals($expected, $a + $b);
    }

    public function additionProvider()
    {
        return [
           [0, 0, 0],
           [0, 1, 1],
           [1, 0, 1],
           [1, 1, 2]
       ];
    }

    public function testEquality()
    {
        $this->assertEquals(1, '1');

        $this->assertEquals(
            [1, 2,  3, 4, 5, 6],
            ['1', 2, 3, 4, 5, 6]
        );
    }
}
